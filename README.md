# TSP service

A demo backend service that uses Google's "[OR-Tools](https://developers.google.com/optimization)" library for solving instances of the [Traveling Salesman Problem](https://developers.google.com/optimization/routing/tsp#python_5).

- `tsp/` - the service (Python package)
- `tsp-client/` - CLI demo client (also Python package)
- `rabbitmq/` - dev configs for RabbitMQ

The Python packages are managed with [Hatch](https://hatch.pypa.io/latest/).

> When there are [so many Python packaging tools](https://packaging.python.org/en/latest/key_projects/), which one has the cutest logo?

## Running

Start services:

    $ docker compose --env-file '.env' up --detach

It will take a minute for the RabbitMQ health check to pass, then the service instances will start. `.env` contains environment variables for connecting to RabbitMQ and the number of replicas and CPUs to use.

> To go _more parallel_, set `TSP_REPLICAS` and `TSP_CPUS_EACH` so that `replicas x cpus = <your CPU cores>`.

Running the demo client:

    $ cd ./tsp-client
    $ hatch run app --help

You can specify all the connection options with either flags or environment variables, but most of them have defaults that match the development environment / service defaults. The only one that must be specified is `-w` / `$TSP_BROKER_PASS`

    $ hatch run app -w $PASS send "(0,0, 'a')" "(-2, -1, 'b')" "(1, 2, 'c')"

That sends a single request to the input queue and waits for athe corresponding response on the output queue.

More interesting:

    $ hatch run app -w $PASS flood 12 4

The first argumet is the total number of requests to send, the second is the number of requests to allow in-flight at a time.
e.g., the above will send 4 requests and wait until at least one response is received before sending the fifth.

To cause load, set the second number to at least the number of `replicas x cpus` the services were configured with, to keep all your cores busy. You can also increase the number of randomly generated points to include in each request, e.g.

    $ hatch run app -w $PASS flood --max-route-len 30 50 8

Note if the services are still processing jobs from other / previous client requests, the active flood command may receive those, causing a mismatch between the number of requests sent vs. responses received. `asyncio.BoundedSemaphore` exceptions are because of that.
