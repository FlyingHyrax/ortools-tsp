import logging
from collections.abc import Callable
from dataclasses import dataclass
from datetime import timedelta
from functools import lru_cache
from math import sqrt

from ortools.constraint_solver import pywrapcp, routing_enums_pb2
from ortools.init import pywrapinit

from tsp.dto import Point

LOGGER = logging.getLogger(__name__)

def init_ortools(usage: str) -> None:
    pywrapinit.CppBridge.InitLogging(usage)


def euclidean_distance(p: Point, q: Point) -> int:
    if p == q:
        return 0
    x_d = p.x - q.x
    y_d = p.y - q.y
    fp_dist = sqrt(x_d**2 + y_d**2)
    return round(fp_dist)


def create_arc_cost_callback(
    points: list[Point],
    index_manager: pywrapcp.RoutingIndexManager,
    distance_impl: Callable[[Point, Point], int] = euclidean_distance,
):
    @lru_cache
    def get_distance(p_from: Point, p_to: Point) -> int:
        return distance_impl(p_from, p_to)

    def cost_func(orig_routing_idx, dest_routing_idx):
        # convert from routing variable index to distance matrix index
        orig_point_idx = index_manager.IndexToNode(orig_routing_idx)
        dest_point_idx = index_manager.IndexToNode(dest_routing_idx)
        return get_distance(points[orig_point_idx], points[dest_point_idx])

    return cost_func


@dataclass
class RoutingSolution:
    path: list[Point]
    distance: int
    objective_value: int

_TEN_SECONDS =  timedelta(seconds=10)

def solve_instance(
    points: list[Point],
    start_point: int = 0,
    search_time_limit: timedelta = _TEN_SECONDS
) -> RoutingSolution:
    LOGGER.debug("configuring solver")

    # untyped positional args:
    # (1) number of nodes
    # (2) number of vehicles
    # (3) index of start node
    index_manager = pywrapcp.RoutingIndexManager(len(points), 1, start_point)
    routing_model = pywrapcp.RoutingModel(index_manager)

    get_arc_cost = create_arc_cost_callback(points, index_manager)
    transit_callback_handle = routing_model.RegisterTransitCallback(get_arc_cost)
    # this makes the cost the same for all vehicles (if we had more than one)
    routing_model.SetArcCostEvaluatorOfAllVehicles(transit_callback_handle)

    search_params = pywrapcp.DefaultRoutingSearchParameters()
    search_params.first_solution_strategy = routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC
    search_params.local_search_metaheuristic = routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH
    search_params.time_limit.seconds = int(search_time_limit.total_seconds())

    # execute the search
    LOGGER.debug("executing solver")
    solution: pywrapcp.Assignment = routing_model.SolveWithParameters(search_params)
    objective = solution.ObjectiveValue()
    LOGGER.debug(f"solution found with objective value {objective}")

    # TODO: refactor into function
    vehicle_idx = 0
    route = []
    route_distance = 0

    model_idx = routing_model.Start(vehicle_idx)
    while not routing_model.IsEnd(model_idx):
        point_idx = index_manager.IndexToNode(model_idx)
        point = points[point_idx]
        route.append(point)
        prev_model_idx = model_idx
        model_idx = solution.Value(routing_model.NextVar(model_idx))
        route_distance += routing_model.GetArcCostForVehicle(prev_model_idx, model_idx, vehicle_idx)
    # add the end point
    route.append(points[index_manager.IndexToNode(model_idx)])

    return RoutingSolution(
        path=route,
        distance=route_distance,
        objective_value=objective,
    )
