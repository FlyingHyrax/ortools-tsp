import asyncio
import logging
import sys
from concurrent.futures import Executor, ProcessPoolExecutor

import aio_pika
from aio_pika.abc import AbstractChannel, AbstractIncomingMessage

from tsp import environment
from tsp import logging as logging_config
from tsp.dto import VehicleRouteRequest, VehicleRouteResponse, cbor_decode, cbor_encode
from tsp.solver import init_ortools, solve_instance

LOGGER = logging.getLogger(__name__)

EXIT_OK = 0
EXIT_ERROR = -1


class MessageHandler:
    """
    Contain dependencies for receiving requests, processing them concurrently in a worker pool, and
    sending responses. Does *not* handle cleanup of context-like objects e.g. Channels or Executors.
    Should use context managers or try/finally outside this class before it is instantiated.
    """
    def __init__(
        self,
        event_loop: asyncio.AbstractEventLoop,
        task_group: asyncio.TaskGroup,
        executor: Executor,
        concurrent_messages: int,
        channel: AbstractChannel,
        response_queue: str,
    ) -> None:
        """Initialize a message handler instance. Assumes that the given event loop is already running,
        the aio_pika Channel is already connected, and the named RMQ queue already exists.

        The task group is used to create concurrent tasks that each use the event loop to run the solver
        on the executor. If you only use the executor, you end up running tasks sequentially because
        "run_in_executor" waits for the work to be *finished*, not only for it to start.

        :param asyncio.AbstractEventLoop event_loop: event loop for dispatching to the executor
        :param asyncio.TaskGroup task_group: task group to create tasks with
        :param Executor executor: a concurrent.futures Executor
        :param int concurrent_messages: number of tasks allowed to be processing at once.
                Should probably match the size of the Executor's worker pool
        :param AbstractChannel channel: RMQ channel for sending response messages
        :param str response_queue: RMQ queue to send responses to
        """
        self.event_loop = event_loop
        self.task_group = task_group
        self.executor = executor
        self.channel = channel
        self.response_queue_name = response_queue

        self.running_tasks = set()
        self.token_bucket = asyncio.BoundedSemaphore(value=concurrent_messages)

    async def handle_message(self, request_msg: AbstractIncomingMessage):
        # controls the number of running tasks; will not start a new task
        # unless we are below the max concurrent number
        await self.token_bucket.acquire()

        LOGGER.debug("received message")

        try:
            obj = cbor_decode(request_msg.body)
            if not isinstance(obj, VehicleRouteRequest):
                msg = "decoded object was not an instance of VehicleRouteRequest"
                raise ValueError(msg)

            LOGGER.debug("parsed request %s", obj.uuid)

            # Start a task to process the request and keep a strong reference to
            #  the task while it is running (thx asyncio)
            task = self.task_group.create_task(self.process_request(obj))
            self.running_tasks.add(task)

            # when the task ends, remove it from the collection
            # and release a token so another task can proceed
            def on_task_finish(task: asyncio.Task):
                self.running_tasks.discard(task)
                self.token_bucket.release()

            task.add_done_callback(on_task_finish)

            # lastly, ack the message b/c it deocded to a valid request
            await request_msg.ack()

        except Exception as exc:
            LOGGER.warning("unable to parse message body: %s", exc)

            # we couldn't parse the message so other instances of the same service won't
            # be able to either; reject the message and tell RMQ to drop it
            await request_msg.reject(requeue=False)

            # only release this on error; otherwise we want the running task
            # to count as a request-in-process and only release it when the
            # task is finished
            self.token_bucket.release()

    async def process_request(self, request: VehicleRouteRequest):
        try:
            LOGGER.debug("dispatching request to executor")
            result = await self.event_loop.run_in_executor(
                self.executor,
                solve_instance,
                request.destinations,
                request.origin_index,
            )
            # We don't reach here until the background process finishes
            # running the function; i.e. this doesn't implicitly create
            # a task - why we use both a TaskGroup and an Executor

            response = VehicleRouteResponse(request.uuid, result.path)
            await self.send_response(response)

        except Exception:
            # Not a lot we can do at this point unless/until we add an ErrorResponse message type
            # TODO ...add a VehicleRouteError DTO type
            LOGGER.exception("error while processing request %s", request.uuid)

    async def send_response(self, response: VehicleRouteResponse):
        # exceptions thrown in here will be caught in process_request
        body = cbor_encode(response)
        message = aio_pika.Message(
            body, content_type="application/cbor", delivery_mode=aio_pika.DeliveryMode.PERSISTENT
        )

        LOGGER.debug("sending response for %s", response.uuid)
        await self.channel.default_exchange.publish(message, self.response_queue_name)


async def process_requests():
    loop = asyncio.get_event_loop()

    LOGGER.info("connecting to message broker")

    # TODO: retry with backoff on connection failure
    connection = await aio_pika.connect_robust(
        host=environment.BROKER_HOST,
        port=environment.BROKER_PORT,
        login=environment.BROKER_USERNAME,
        password=environment.BROKER_PASSWORD,
        virtualhost=environment.BROKER_VHOST,
        loop=loop,
    )

    async with connection:
        # Bind these variables so we can use them in a 'finally' block;
        # needed for anything we aren't using as a context manager
        recv_channel, recv_queue, send_channel, consumer_tag = None, None, None, ""

        try:
            LOGGER.debug("setting up channels and queues")

            recv_channel = await connection.channel()
            _qos_ok = await recv_channel.set_qos(prefetch_count=environment.BROKER_PREFETCH_COUNT)
            recv_queue = await recv_channel.declare_queue(environment.BROKER_TASK_QUEUE_NAME, durable=True)

            send_channel = await connection.channel()
            send_queue = await send_channel.declare_queue(environment.BROKER_RESULT_QUEUE_NAME, durable=True)

            concurrent_count = environment.WORKERS

            with ProcessPoolExecutor(max_workers=concurrent_count) as executor:
                async with asyncio.TaskGroup() as group:
                    message_processor = MessageHandler(
                        event_loop=loop,
                        task_group=group,
                        executor=executor,
                        concurrent_messages=concurrent_count,
                        channel=send_channel,
                        response_queue=send_queue.name,
                    )

                    LOGGER.info("completed setup and ready to receive requests")
                    consumer_tag = await recv_queue.consume(message_processor.handle_message)

                    # this never completes, so is like permanently yielding to other tasks
                    await asyncio.Future()
        finally:
            if recv_queue and consumer_tag:
                await recv_queue.cancel(consumer_tag)
            if recv_channel:
                await recv_channel.close()
            if send_channel:
                await send_channel.close()


def main(_argv: list[str]) -> int:
    logging_config.setup_primary_logging()
    mp_logging_listener = logging_config.setup_multiprocess_logging()

    LOGGER.info("service starting")

    init_ortools(__name__)

    try:
        # TODO: handle SIGINT, SIGTERM, esp whatever 'docker compose stop' sends
        mp_logging_listener.start()
        asyncio.run(process_requests())
        return EXIT_OK

    except KeyboardInterrupt:
        LOGGER.info("received keyboard interrupt, exiting")
        return EXIT_OK

    except Exception:
        LOGGER.exception("unhandled exception!", exc_info=True, stack_info=True)
        return EXIT_ERROR

    finally:
        mp_logging_listener.stop()


if __name__ == "__main__":
    sys.exit(main(sys.argv))
