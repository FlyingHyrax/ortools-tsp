import logging
import logging.config
from logging.handlers import QueueHandler, QueueListener
from multiprocessing import Queue as Mp_Queue

from tsp import environment

_MULTILINE = """\
%(asctime)s [%(name)s : %(levelname)s] P%(process)d:T%(thread)d
  %(message)s

"""

_CONFIG = {
    "version": 1,
    "formatters": {
        "brief": {
            "format": "%(asctime)s (%(levelname)-8s) <%(processName)s> %(message)s",
            "datefmt": r"%Y-%m-%d %H:%M:%S",
        },
        "verbose": {
            "format": _MULTILINE,
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "brief",
            "level": "DEBUG",
            "stream": "ext://sys.stdout",
        },
        "file": {
            "class": "logging.FileHandler",
            "formatter": "verbose",
            "level": "ERROR",
            "filename": "errors.log",
            "delay": True,
            "encoding": "utf-8",
        },
    },
    "loggers": {
        "tsp": {
            "level": "DEBUG" if environment.DEBUG else "INFO",
            "handlers": ["console", "file"],
            "propagate": False,
        },
    },
    "root": {
        "level": "DEBUG" if environment.DEBUG else "WARNING",
        "handlers": ["console"],
    }
}


def setup_primary_logging():
    logging.config.dictConfig(_CONFIG)

def setup_multiprocess_logging() -> QueueListener:
    msg_queue = Mp_Queue()
    mp_handler = QueueHandler(msg_queue)

    worker_logger = logging.getLogger("tsp.solver")
    worker_logger.addHandler(mp_handler)
    worker_logger.propagate = False

    main_handlers = logging.getLogger("tsp").handlers

    return QueueListener(msg_queue, *main_handlers, respect_handler_level=True)
