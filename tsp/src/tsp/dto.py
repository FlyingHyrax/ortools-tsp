from dataclasses import dataclass
from typing import Any, ClassVar, Protocol, Self
from uuid import UUID

import cbor2
from cbor2 import CBORDecoder, CBOREncoder, CBORTag

# Using the cbor2's support for encoding arbitrary types with custom tag numbers;
# see <https://cbor2.readthedocs.io/en/latest/customizing.html>.
# Chosen tag numbers fall in the currently unassigned range 60003-65534.

class CborEncodable(Protocol):
    """Type that can be converted to/from CBOR using a custom tag number.
    Chosen tag numbers should avoid tags already registered with the IANA;
    see <https://www.iana.org/assignments/cbor-tags/cbor-tags.xhtml>.
    """
    tag_number: ClassVar[int]

    def to_cbor(self: Self) -> CBORTag:
        ...

    @classmethod
    def from_cbor(cls: type[Self], tagged_value: CBORTag) -> Self:
        ...


def _compare_cbor_tag_number(cls: type[CborEncodable], tv: CBORTag):
    """Compare the tag number of a class to that of a tagged value and throw an exception if they don't match."""
    if cls.tag_number != tv.tag:
        msg = f"error decoding {cls.__name__} from CBOR - tag mismatch (expected: {cls.tag_number}, actual: {tv.tag})"
        raise ValueError(msg)


def _get_value_checked(cls: type[CborEncodable], tv: CBORTag) -> dict:
    """Get the value from a tagged value; throw an exception if that value is not a dict instance."""
    data = tv.value
    if not isinstance(data, dict):
        msg = f"error decoding {cls.__name__} from CBOR - inner tagged value was not a dict"
        raise ValueError(msg)
    return data


@dataclass(frozen=True)
class Point:
    """A point in 2-dimensional euclidean space with an optional text label."""
    x: int
    y: int
    label: str | None = None

    tag_number: ClassVar[int] = 0xEE01

    def to_cbor(self) -> CBORTag:
        return CBORTag(tag=self.tag_number, value=vars(self))

    @classmethod
    def from_cbor(cls, tagged_value: CBORTag) -> "Point":
        _compare_cbor_tag_number(cls, tagged_value)
        params = _get_value_checked(cls, tagged_value)
        return Point(**params)


@dataclass
class VehicleRouteRequest:
    """Service request message for the TSP service - defines an input to the traveling-salesman problem.
    """
    uuid: UUID
    origin_index: int
    destinations: list[Point]

    tag_number: ClassVar[int] = 0xEE02

    def to_cbor(self) -> CBORTag:
        return CBORTag(
            tag=self.tag_number,
            value=vars(self),
        )

    @classmethod
    def from_cbor(cls, tagged_value: CBORTag) -> "VehicleRouteRequest":
        _compare_cbor_tag_number(cls, tagged_value)
        params = _get_value_checked(cls, tagged_value)
        return VehicleRouteRequest(**params)


@dataclass
class VehicleRouteResponse:
    """Service response message containing a solution to an instance of the traveling-salesman problem.
    """
    uuid: UUID
    route: list[Point]

    tag_number: ClassVar[int] = 0xEE03

    def to_cbor(self) -> CBORTag:
        return CBORTag(
            tag=self.tag_number,
            value=vars(self),
        )

    @classmethod
    def from_cbor(cls, tagged_value: CBORTag) -> "VehicleRouteResponse":
        _compare_cbor_tag_number(cls, tagged_value)
        params = _get_value_checked(cls, tagged_value)
        return VehicleRouteResponse(**params)


def encode_custom_types(encoder: CBOREncoder, value: Any):
    """Implementation of cbor2.CBOREncoder's "default_encoder" hook. The encoder calls that hook
    with any Python value it does not know how to serialize. If the value seems compatible with
    our CborEncodable protocol, we use that to encode the value.

    :param CBOREncoder encoder: active encoder instance
    :param Any value: value to encode
    :raises ValueError: if the value does not support CborEncodable
    """
    if not (hasattr(value, "to_cbor") and callable(value.to_cbor)):
        msg = f"object {value!r} of type {type(value)} missing CBOR encoding method 'to_cbor'"
        raise ValueError(msg)

    encoder.encode(value.to_cbor())


_tag_lookup: dict[int, type[CborEncodable]] = {
    Point.tag_number: Point,
    VehicleRouteRequest.tag_number: VehicleRouteRequest,
    VehicleRouteResponse.tag_number: VehicleRouteResponse,
}


def decode_custom_types(_d: CBORDecoder, tag: CBORTag) -> Any:
    """Implementation of cbor2.CBORDecoder's "tag_hook" parameter. The decoder calls the hook
    for any tagged value that doesn't have a built-in decoder. If the tag number matches one
    of our known CborEncodable types, we use the corresponding class to decode the object.

    This implementation does not support value sharing.

    :param CBORDecoder _d: active decoder instance (currently unused)
    :param CBORTag tag: tagged value representing a custom object
    :raises ValueError: if the tag number doesn't match a known class
    :return Any: decoded object instance
    """
    if tag.tag not in _tag_lookup:
        msg = f"no custom decoder for CBOR tag {tag.tag}'"
        raise ValueError(msg)

    return _tag_lookup[tag.tag].from_cbor(tag)


def cbor_encode(obj: Any) -> bytes:
    """Wrapper around `cbor2.dumps` with a preconfigured default encoder for encoding TSP custom types.

    :param Any obj: object to encode as CBOR
    :return bytes: encoded bytes
    """
    return cbor2.dumps(obj, default=encode_custom_types)


def cbor_decode(data: bytes) -> Any:
    """Wrapper around `cbor2.loads` with a precofigured tag hook for decoding TSP custom types.

    :param bytes data: cbor data to decode
    :return Any: decoded object
    """
    return cbor2.loads(data, tag_hook=decode_custom_types)
