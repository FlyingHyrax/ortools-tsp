from collections.abc import Callable
from os import environ
from typing import TypeVar

T = TypeVar("T")


def read_env(key: str, convert: Callable[[str], T] = str, default: T | None = None) -> T:
    """Read a value from an environment variable, optionally converting it to a different type.

    The 'key' (environment variable name) is not modified in any way before querying the environment.
    i.e. if the desired variable is in upper case, the key must be given in upper case.

    :param str key: an environment variable name
    :param Callable[[str], T] convert: conversion function from string to desired type, defaults to str
    :param T | None default: default value, defaults to None
    :raises KeyError: if the environment variable is missing and no default was specified
    :raises ValueError: if an exception is thrown while converting the value
    :return T: converted value for the specified environment variable name
    """
    value = environ.get(key, default)
    # not in environment and no default given
    if value is None:
        msg = f"missing value for environment variable '{key}'"
        raise KeyError(msg)

    try:
        if isinstance(value, str):
            return convert(value)
        else:
            return value
    except Exception as exc:
        msg = f"failed to convert string value from environment variable '{key}' with convert fn {convert.__qualname__}"
        raise ValueError(msg) from exc


_custom_falsey_strings = set([
    "false", "0", ""
])

def parse_boolean(value: str) -> bool:
    """Convert a string value from an environment variable to a boolean.

    The value is first stripped of surrounding whitespace and converted to lower case.
    The strings 'false', '0', or the empty string resolves to False; all other values resolve to True.

    :param str value: string value to convert
    :return bool: False for "false-like" strings, and True otherwise
    """
    cleaned = value.strip().lower()
    return cleaned not in _custom_falsey_strings


DEBUG: bool = read_env("TSP_DEBUG", convert=parse_boolean, default=False)
WORKERS: int = read_env("TSP_WORKERS", convert=int, default=2)

BROKER_USERNAME: str = read_env("TSP_BROKER_USER")
BROKER_PASSWORD: str = read_env("TSP_BROKER_PASS")
BROKER_HOST: str = read_env("TSP_BROKER_HOST", default="localhost")
BROKER_PORT: int = read_env("TSP_BROKER_PORT", convert=int, default=5672)
BROKER_VHOST: str = read_env("TSP_BROKER_VHOST", default="/")

BROKER_TASK_QUEUE_NAME: str = read_env("TSP_BROKER_TASK_QUEUE_NAME", default="tsp-input-q")
BROKER_PREFETCH_COUNT: int = read_env("TSP_BROKER_PREFETCH_COUNT", convert=int, default=1)
BROKER_RESULT_QUEUE_NAME: str = read_env("TSP_BROKER_RESULT_QUEUE_NAME", default="tsp-output-q")
