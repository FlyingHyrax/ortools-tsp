from tsp.solver import Point, solve_instance


def test_single_point():
    home = Point(0, 0, "Home")
    route = solve_instance([home])
    assert route.distance == 0

    # out of the driveway, then back in
    assert route.path == [home, home]


def test_single_edge():
    orig = Point(0, 0, "Origin")
    dest = Point(3, 4, "Destination")
    route = solve_instance([orig, dest])
    assert route.distance == 10  # there and back
    assert route.path == [orig, dest, orig]


def test_straight_line():
    # 0    5    10
    # |-|--|---||
    # A B  C   9
    points = [
        Point(0, 0, "A"),
        Point(2, 0, "B"),
        Point(5, 0, "C"),
        Point(9, 0, "D"),
    ]
    route = solve_instance(points)
    assert route.distance == 18
    assert [p.label for p in route.path] == ["A", "B", "C", "D", "A"]
