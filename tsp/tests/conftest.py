import pytest

from tsp.solver import init_ortools


@pytest.fixture(autouse=True, scope="session")
def setup_ortools():
    init_ortools("api.tests")
