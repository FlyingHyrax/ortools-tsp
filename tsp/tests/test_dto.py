# ruff: noqa: N802

from uuid import uuid4

import pytest
from cbor2 import dumps, loads

from tsp.dto import Point, VehicleRouteRequest, VehicleRouteResponse, decode_custom_types, encode_custom_types


@pytest.mark.parametrize("pt", [
    Point(0, 0),
    Point(1, 2, "a test label"),
])
def test_serdes_identity_Point(pt: Point):
    dat = dumps(pt, default=encode_custom_types)
    obj = loads(dat, tag_hook=decode_custom_types)
    assert obj == pt

@pytest.mark.parametrize("req", [
    VehicleRouteRequest(uuid=uuid4(), origin_index=0, destinations=[]),
    VehicleRouteRequest(uuid=uuid4(), origin_index=0, destinations=[
        Point(0, 0, "a"),
        Point(1, 1, "b"),
        Point(-1, -1),
    ]),
])
def test_serdes_identity_VehicleRouteRequest(req: VehicleRouteRequest):
    dat = dumps(req, default=encode_custom_types)
    obj = loads(dat, tag_hook=decode_custom_types)
    assert obj == req


@pytest.mark.parametrize("res", [
    VehicleRouteResponse(uuid=uuid4(), route=[]),
    VehicleRouteResponse(uuid=uuid4(), route=[
        Point(1, 1),
        Point(0, 0),
        Point(-1, -1),
    ]),
])
def test_serdes_identity_VehicleRouteResponse(res: VehicleRouteResponse):
    dat = dumps(res, default=encode_custom_types)
    obj = loads(dat, tag_hook=decode_custom_types)
    assert obj == res
