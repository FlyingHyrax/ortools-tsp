# TSP service - Python package

A demo backend service that uses Google's "[OR-Tools](https://developers.google.com/optimization)" library for solving instances of the [Traveling Salesman Problem](https://developers.google.com/optimization/routing/tsp#python_5).

This is the just the service itself; using it requires a RabbitMQ instance and a client to send jobs to the service.

## Development

This is a Python package developed using [Hatch](https://hatch.pypa.io/latest/). You will want to install Hatch in your environment, see <https://hatch.pypa.io/latest/install/>. The package configuration is based on Hatch's defaults; e.g. the linting rules are all from that. Commands for the basics:

- Build: `hatch build` (output in `./dist/`)
- Test: `hatch run test`
- Run: `hatch run app`

If not using Hatch for some reason, the package's primary runtime dependencies are:

- `ortools ~= 9.6`
- `cbor2 ~= 5.4`
- `aio_pika ~= 9.0`

I found this configuration helpful when working in Visual Studio Code:

1. For Hatch, `hatch config set dirs.env.virtual '.venv'` (or your choice or relative path) - this tells Hatch to create virtual environmeents inside the project folder ([docs](https://hatch.pypa.io/latest/config/hatch/#environments))
2. For VS Code, `"python.venvPath": "${workspaceFolder}/*/.venv"` (or similar, you want this to point to where hatch creates the folder from (1))

## Organization

- `src/tsp/` - Python module
    - `dto.py` - "Data Transfer Objects" - classes used to communicate with the service - and serializing/deserializing them
    - `environment.py` - service configuration from environment variables
    - `logging.py` - logging configuration (uses Python's built-in `logging` module)
    - `service.py` - primary logic including `main` function, event loops, and interfacing with RabbitMQ
    - `solver.py` - wrapping functions around `ortools`
    - `__main__.py` - supports starting the service with `python -m tsp`
- `tests/` - unit tests, runnable with `pytest` (or `hatch run test`)
- `Dockerfile` - service container definition
- `pyproject.toml` - Python packaging configuration and metadata; configured with Hatch.

## Configuration

The service is configured with environment variables. All variables, their types, and their defaults are listed in `src/tsp/environment.py`. Most have default values reasonable for local development. The `TSP_BROKER_USER` and `TSP_BROKER_PASS` _do not have defaults_ and must be specified.

