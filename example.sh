#!/usr/bin/bash
set -x

if [! $(which hatch)]; then
    echo "Missing hatch!"
    exit 1
fi

source .env.dev
docker-compose --env-file .env.dev up --detach

pushd tsp_client
hatch run app flood --max-duration 50 12 4
popd

set +x