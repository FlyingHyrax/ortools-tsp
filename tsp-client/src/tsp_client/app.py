# ruff: noqa: UP007

import asyncio
import logging
import sys
from typing import Annotated, Optional

import typer
from rich.console import Console
from rich.logging import RichHandler
from tsp.dto import Point

from tsp_client.flood_cmd import run_drain, run_flood
from tsp_client.rmq import RmqParams
from tsp_client.send_cmd import send_request
from tsp_client.shared import format_route

LOGGER = logging.getLogger(__name__)

console = Console()
app = typer.Typer()

connection_parameters: RmqParams

def parse_point_argument(value: str) -> Point:
    obj = eval(value)
    try:
        return Point(*obj)
    except Exception as exc:
        msg = f"unable to parse point from '{value}'; expected format (x, y, \"label\")"
        raise ValueError(msg) from exc


def configure_logging(console: Console, level: int):
    handler = RichHandler(console=console)
    logger = logging.getLogger("tsp_client")
    logger.addHandler(handler)
    logger.setLevel(level)


@app.command("send")
def send_cmd(points: Annotated[list[Point], typer.Argument(parser=parse_point_argument)]):
    """Send a single request"""
    LOGGER.debug("parsed points: %r", points)
    response = asyncio.run(send_request(connection_parameters, points))
    LOGGER.info("route: %s", format_route(response.route))


@app.command("flood")
def flood_cmd(
    count: Annotated[int, typer.Argument(min=1, help="Total number of requests to send")],
    concurrent: Annotated[int, typer.Argument(min=1, max=128, clamp=True, help="Number of concurrent requests")],
    pause: Annotated[int, typer.Option(min=0, help="Seconds to pause between requests")] = 1,
    max_route_len: Annotated[int, typer.Option(min=1, max=100, help="Max number of stops in randomly generated routes")] = 10,
    max_duration: Annotated[Optional[int], typer.Option(help="Max time for sending requests; default until 'count'' have been sent")] = None,
):
    """Send a lot of randomly generated requests"""
    LOGGER.debug(
        "parameters: count %d, concurrent %d, pause: %d, max route len: %d, max duration: %r",
        count,
        concurrent,
        pause,
        max_route_len,
        max_duration,
    )
    asyncio.run(
        run_flood(
            connection_parameters,
            count,
            concurrent,
            pause,
            max_route_len,
            max_duration,
        )
    )


@app.command("drain")
def drain_cmd(timeout: int = 10):
    """Consume messages from the TSP service output queue. Exits when there are no new messages for 'timeout' seconds."""
    asyncio.run(run_drain(connection_parameters, timeout))

@app.callback()
def root(
    host: Annotated[str, typer.Option("-h", envvar="TSP_BROKER_HOST", )] = "localhost",
    port: Annotated[int, typer.Option("-p", envvar="TSP_BROKER_PORT")] = 5672,
    vhost: Annotated[str, typer.Option("-v", envvar="TSP_BROKER_VHOST")] = "tsp_app",
    username: Annotated[str, typer.Option("-u", envvar="TSP_BROKER_USER", help="user to connect with")] = "tsp_app",
    password: Annotated[str, typer.Option("-w", envvar="TSP_BROKER_PASS", help="password for user")] = "",
    queues: Annotated[
        tuple[str, str], typer.Option("-q", help="queue names; service input queue and service output queue")
    ] = ("tsp-input-q", "tsp-output-q"),
):
    global connection_parameters  # noqa: PLW0603
    connection_parameters = RmqParams(host, port, vhost, username, password, queues)

    configure_logging(console, logging.DEBUG)  # TODO parameterize
    LOGGER.debug("active connection options: %r", connection_parameters)


def main() -> int:
    try:
        app()
        return 0
    except Exception:
        LOGGER.exception("unhandled exception")
        return -1

if __name__ == "__main__":
    sys.exit(main())
