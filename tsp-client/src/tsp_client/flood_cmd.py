# ruff: noqa: S311
# ruff: noqa: UP007
import asyncio
import logging
import random
import uuid
from collections.abc import Iterable
from typing import Optional

import aio_pika
import aio_pika.exceptions
from tsp.dto import Point, VehicleRouteRequest, VehicleRouteResponse, cbor_decode, cbor_encode

from tsp_client.rmq import RmqParams, get_connection
from tsp_client.shared import format_route

LOGGER = logging.getLogger(__name__)


class IdentGenerator:
    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    alphabet_size = len(alphabet)

    def __init__(self):
        self.count = 0

    def get(self) -> str:
        letters: list[str] = []
        value = self.count

        def next_letter():
            nonlocal value
            idx = value % self.alphabet_size
            letters.append(self.alphabet[idx])
            value = value // self.alphabet_size

        # no do-while
        next_letter()
        while value > 0:
            next_letter()

        self.count += 1
        return "".join(reversed(letters))


def generate_points(count: int) -> list[Point]:
    generated_coords = set()
    name_gen = IdentGenerator()

    while len(generated_coords) < count:
        rx = random.randint(-100, 100)
        ry = random.randint(-100, 100)
        coord = (rx, ry)
        if coord in generated_coords:
            continue
        else:
            generated_coords.add(coord)

    return [Point(x, y, label=name_gen.get()) for (x, y) in generated_coords]


def request_generator(count: int, max_route_len: int) -> Iterable[VehicleRouteRequest]:
    for _ in range(0, count):
        point_count = random.randint(3, max_route_len)
        points = generate_points(point_count)
        yield VehicleRouteRequest(
            uuid=uuid.uuid4(),
            origin_index=0,
            destinations=points,
        )


async def send_request(
        n: int,
        req: VehicleRouteRequest,
        queue_name: str,
        channel: aio_pika.abc.AbstractChannel,
    ) -> uuid.UUID:

    LOGGER.debug("sending request %d (%s) with %d stops", n, req.uuid, len(req.destinations))

    body = cbor_encode(req)
    message = aio_pika.Message(body=body)
    await channel.default_exchange.publish(message, queue_name)

    return req.uuid


async def listen_for_responses(queue: aio_pika.abc.AbstractQueue, token_bucket: asyncio.BoundedSemaphore, started_sending: asyncio.Event):

    async def message_received(message: aio_pika.abc.AbstractIncomingMessage):
        obj = cbor_decode(message.body)
        if not isinstance(obj, VehicleRouteResponse):
            LOGGER.warning("received invalid message on output queue, dropping")
            await message.reject()
        else:
            await message.ack()

        LOGGER.info(f"received response {obj.uuid!s}: {format_route(obj.route)}")

        if started_sending.is_set():
            token_bucket.release()

    tag = await queue.consume(message_received)
    LOGGER.info("listening for responses")
    return tag


async def generate_requests(
        count: int,
        pause: int,
        queue_name: str,
        channel: aio_pika.abc.AbstractChannel,
        max_route_len: int,
        token_bucket: asyncio.BoundedSemaphore,
        started_sending: asyncio.Event,
    ) -> None:

    LOGGER.info("generating requests")
    started_sending.set()
    async with asyncio.TaskGroup() as group:
        for n, req in enumerate(request_generator(count, max_route_len)):
            if pause > 0:
                await asyncio.sleep(pause)
            await token_bucket.acquire()
            group.create_task(send_request(n, req, queue_name, channel))


async def run_flood(
        params: RmqParams,
        req_count: int,
        concurrent_requests: int,
        pause_send: int,
        max_route_len: int,
        max_duration: Optional[int],
    ):

    connection = await get_connection(params)
    input_q_name, output_q_name = params.queues

    # because we start listening for responses before starting to send requests,
    # this can be released before being acquired...
    token_bucket = asyncio.BoundedSemaphore(value=concurrent_requests)
    started_sending = asyncio.Event()

    async with connection:
        channel = await connection.channel()
        async with channel:
            output_queue = await channel.get_queue(output_q_name, ensure=True)
            consumer_tag = await listen_for_responses(output_queue, token_bucket, started_sending)

            try:
                async with asyncio.timeout(max_duration):
                    await generate_requests(
                        req_count,
                        pause_send,
                        input_q_name,
                        channel,
                        max_route_len,
                        token_bucket,
                        started_sending,
                    )
            except asyncio.TimeoutError:
                return
            finally:
                await output_queue.cancel(consumer_tag)



def build_consumer(event: asyncio.Event):
    async def consume_message(message: aio_pika.abc.AbstractIncomingMessage):
        event.set()
        await message.ack()

        obj = cbor_decode(message.body)
        if not isinstance(obj, VehicleRouteResponse):
            LOGGER.warning("consumed unrecognized message type %r", obj)
        else:
            LOGGER.info("response %s: %s", obj.uuid, format_route(obj.route))

    return consume_message

async def run_drain(params: RmqParams, timeout: int):
    connection = await get_connection(params)
    _, output_queue_name = params.queues

    async with connection:
        LOGGER.debug("connected")
        channel = await connection.channel()
        async with channel:
            LOGGER.debug("channel open")
            output_queue = await channel.get_queue(output_queue_name, ensure=True)
            event = asyncio.Event()
            consumer_token = await output_queue.consume(build_consumer(event))

            LOGGER.info("draining messages from %s", output_queue_name)
            while True:
                try:
                    # start a timeout
                    async with asyncio.timeout(timeout):
                        # release the lock and wait for notify
                        await event.wait()
                        event.clear()
                        # if we reach here, event was triggered, implying
                        # a message was received, so we should restart
                        # the timeout
                        continue

                except asyncio.TimeoutError:
                    LOGGER.info("no responses in queue for %d seconds, exiting", timeout)
                    return
                finally:
                    await output_queue.cancel(consumer_token)
                # propagate all other exceptions
