import asyncio
from typing import NamedTuple

import aio_pika


class RmqParams(NamedTuple):
    host: str
    port: int
    virtual_host: str
    username: str
    password: str
    queues: tuple[str, str]

async def get_connection(params: RmqParams) -> aio_pika.abc.AbstractConnection:
    loop = asyncio.get_event_loop()

    connection = await aio_pika.connect(
        host=params.host,
        port=params.port,
        login=params.username,
        password=params.password,
        virtualhost=params.virtual_host,
        loop=loop,
    )

    return connection
