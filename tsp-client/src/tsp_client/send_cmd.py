import logging
import uuid

import aio_pika
from tsp.dto import Point, VehicleRouteRequest, VehicleRouteResponse, cbor_decode, cbor_encode

from tsp_client.rmq import RmqParams, get_connection

LOGGER = logging.getLogger(__name__)

async def _wait_for_response(request_id: uuid.UUID, queue: aio_pika.abc.AbstractQueue) -> VehicleRouteResponse:
    while True:
        # would increase the timeout here, but it doesn't work correctly;
        # will need to make minimal reproducible example for aio_pika
        message = await queue.get(fail=False)
        if message is None:
            # LOGGER.debug("timed out while waiting for a message, trying again")
            continue

        LOGGER.info("received a message: %r", message.message_id)
        obj = cbor_decode(message.body)

        if not isinstance(obj, VehicleRouteResponse):
            LOGGER.warning("received message that wasn't a response type, dropping")
            await message.reject(requeue=False)
            continue

        if obj.uuid != request_id:
            LOGGER.info("received response for a different request, will requeue")
            await message.reject(requeue=True)
            continue

        await message.ack()
        return obj


async def send_request(params: RmqParams, points: list[Point]) -> VehicleRouteResponse:

    connection = await get_connection(params)
    async with connection:
        channel = await connection.channel()

        # ? can these be 'passive' ?
        (svc_input_queue_name, svc_output_queue_name) = params.queues

        svc_output_q = await channel.declare_queue(svc_output_queue_name, durable=True)

        request_id = uuid.uuid4()
        request = VehicleRouteRequest(uuid=request_id, origin_index=0, destinations=points)
        LOGGER.debug("created request %r", request)

        request_message = aio_pika.Message(cbor_encode(request), delivery_mode=aio_pika.DeliveryMode.PERSISTENT)
        LOGGER.info("sending request_message %s", request.uuid)

        confirmation = await channel.default_exchange.publish(request_message, svc_input_queue_name)
        LOGGER.debug("confirmation %r", confirmation)

        LOGGER.info("waiting for response")
        response = await _wait_for_response(request_id, svc_output_q)
        LOGGER.info("received response: %r", response)

        return response
