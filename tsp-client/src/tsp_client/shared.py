# ruff: noqa: UP007
from typing import Optional, TypeVar

from tsp.dto import Point

T = TypeVar("T")

def unwrap(maybe: Optional[T]) -> T:
    if maybe is None:
        msg = "unexpected 'None'"
        raise ValueError(msg)
    return maybe

def format_route(pts: list[Point]) -> str:
    def name(pt: Point) -> str:
        if pt.label is None:
            return f"({pt.x},{pt.y})"
        else:
            return pt.label
    names = [name(pt) for pt in pts]
    return " -> ".join(names)

